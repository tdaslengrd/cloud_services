This directory contains templates for 'preconfigured' monitors which the DG Agent can use for quick provisiong of monioring services.

The following monitors require modifications before install;
iis.d
sqlserver.d

IIS Monitoring (iis.d)
Monitoring for sites is configured on a per website basis. The following needs to be added at the bottom of the configuration file after the logs: tag;
- type: file

  path: C:\inetpub\logs\LogFiles\W3SVC1\u_ex*
  service: iis
  source: iis

  sourcecategory: http_web_access
  tags: "Default Web Site"

To monitor multiple websites, add multiple instances of =type under the logs: tag;
- type: file

  path: C:\inetpub\logs\LogFiles\W3SVC1\u_ex*
  service: iis
  source: iis

  sourcecategory: http_web_access
  tags: www.website1.com
- type: file

  path: C:\inetpub\logs\LogFiles\W3SVC2\u_ex*
  service: iis
  source: iis

  sourcecategory: http_web_access
  tags: www.website2.com


SQL Monitoring (sqlserver.d)
Monitoring of SQL health is performed by a SQL user having appropriate access to the SQL intance.

You need to run the following T-SQL command in your SQL server to setup the user with the correct permissions;
  CREATE LOGIN techdata_monitor WITH PASSWORD = '<RANDOMPASSWORD>';
  CREATE USER techdata_monitor FOR LOGIN techdata_monitor;
  GRANT SELECT on sys.dm_os_performance_counters to techdata_monitor;
  GRANT VIEW SERVER STATE to techdata_monitor;

After the user has been created you can then add the configuration file with the following settings applied;
- host: localhost,1433

  username: techdata_monitor

  password: <RANDOMPASSWORD>
Change <RANDOMPASSWORD> to a random generated password which is at least 15 characters long.

The above assumes that the host: tag is configured to monitor a local SQL server runnig on port 1433. Adjust as required depending on your deployment.

Additionally if there is a requriement to monitor a SQL server which has no internet access, a server which has access on a trust network to the SQL server as well as the internet can be used as a 'proxy' agent.

When using the 'proxy' type deployment, add the following to the configuration file;
  tags:
    - <SERVERNAME>
Change <SERVERNAME> to the SQL servers name, such that all events and monitors reporting to DataDog are unique and not representative of the proxy server.