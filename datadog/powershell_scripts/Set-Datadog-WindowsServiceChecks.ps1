﻿#########################################################
###### DataDog - Windows Service Monitoring Setup #######
#########################################################
### This script automatically configurs the DataDog   ###
### Agent configuration file for windows_service.d    ###
### to monitoring all services under the local system ###
### which are marked for Automatic startup.           ###
#########################################################
### Rerunning the script will automatically update    ###
### the list of services to monitor based on changes  ###
### made to the system.                               ###
#########################################################
### It is recommeneded to run this script at minimum  ###
### once per 12 hours and no more than once per hour. ###
#########################################################
### The settings in the file assume you are running   ###
### DataDog Agent version 6 or above.                 ###
#########################################################

$Test = 1 #Ser this to 1 for TEST mode, and 0 for PROD mode.

if ($Test -eq 1)
{
    Write-Host "Running in TEST mode."
    $DGConfDir = 'C:/etc/conf.d'
    $DGAgentVersion = 6
}
else
{
    # Set the conf.d directory path for DataDog
    $DGConfDir = 'C:/ProgramData/Datadog/conf.d'

    # Chack if DataDog Agent version 6 is installed
    $DGAgentVersion = Get-WmiObject -Class Win32_Product | Where Name -eq "Datadog Agent" | ForEach {$_.Version}
}

# Variables
$File = $DGConfDir+'/windows_service.d/conf.yaml.example'
$outFile = $DGConfDir+'/windows_service.d/conf.yaml'
$searchTest = "      services:"

if ($DGAgentVersion -lt 6)
{
	#Do nothing because an older agent is installed
	write-host "You need to upgrade your version of Datadog before using this script!"
	write-host "Version detected $DGAgentVersion"
	write-host "Exiting!"
	exit
}
else
{
	$edited = 0
	write-host "Datadog version $DGAgentVersion installted!"
	write-host "Getting list of services set to run automatically."

	# Get all services which have the startup type set to Automatic
	$Services = Get-Service | Select -Property * | Where StartType -eq "Automatic" | ForEach { $_.Name }
    $ServiceCount = $Services.count
	write-host "Found $ServiceCount services!"
		
	# Process lines of text from file and assign result to $NewContent variable
	$NewContent = Get-Content -Path $File |
		ForEach-Object {
			# If line matches regex
			if($_ -match $searchTest)
			{
				# Output this line to pipeline
				$_

				# And output additional line right after it
				ForEach ($Service in $Services)
				{
					'        - ' + $Service
				}
			}
			else # If line doesn't matches regex
			{
				$_
			}
		}

    if (Test-Path $outFile)
    {
        Write-Host "Existing conf file detected, checking for differences."
        
        $DiffTest = Compare-Object -ReferenceObject (Get-Content $outFile) -DifferenceObject ($NewContent)
        if ($DiffTest)
        {
            Write-Host "A service must have been changed!"

            writeConf
        }
        else
        {
            Write-Host "No changes detected, exiting!"
        }
    }
    else
    {
        writeConf
    }
}

Function writeConf
{
    Try
    {
        # Write content of $NewContent varibale back to file
        $NewContent | Out-File -FilePath $outFile -Encoding Default -Force 
        write-host "Sucessfully added service entries."

        if ($Test -ne 1)
        {
            #Restart the DataDog Agent to make the changes active
	        Restart-Service -Name DatadogAgent -Force
        }
    }
    Catch
    {
        Write-Host "Something went wrong!"
        Write-Host $_.Exception.Message
        exit
    }
}