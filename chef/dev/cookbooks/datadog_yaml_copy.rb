remote_file '/etc/datadog-agent/conf.d/apache.d/conf.yaml' do
	source 'https://bitbucket.org/tdaslengrd/cloud_services/raw/master/datadog/conf.d/apache.d/conf.yaml'
	action :create
end

remote_file '/etc/datadog-agent/conf.d/mysql.d/conf.yaml' do
	source 'https://bitbucket.org/tdaslengrd/cloud_services/raw/master/datadog/conf.d/mysql.d/conf.yaml'
	action :create
end

remote_file '/etc/datadog-agent/conf.d/php.d/conf.yaml' do
	source 'https://bitbucket.org/tdaslengrd/cloud_services/raw/master/datadog/conf.d/php.d/conf.yaml'
	action :create
end

remote_file '/etc/datadog-agent/conf.d/statsd.d/conf.yaml' do
	source 'https://bitbucket.org/tdaslengrd/cloud_services/raw/master/datadog/conf.d/statsd.d/conf.yaml'
	action :create
end
