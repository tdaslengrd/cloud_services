directory 'C:\etc\scripts' do
  rights :full_control, 'Administrators'
  recursive true
  action :create
  not_if { ::Dir.exists?('C:\etc\scripts') }
end

remote_file 'C:\etc\scripts\MSLicenseUsageAudit.pa1.ps1' do
  source 'https://bitbucket.org/atsservicesrd/dev-test/raw/7a84c61cb0a0036d3bffcf3228aa48dc7f868aad/Scripts/MSLicenseUsageAudit.pa1.ps1'
  action :create
  not_if { ::File.exists?('C:\etc\scripts\MSLicenseUsageAudit.pa1.ps1') }
end

windows_task 'Test2amSchedule' do
  command 'C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe "C:\etc\scripts\MSLicenseUsageAudit.pa1.ps1"'
  run_level :highest
  frequency :daily
  frequency_modifier 1
  start_time "03:20"
  not_if {"schtasks.exe /Query /TN Test2amSchedule"}
end
