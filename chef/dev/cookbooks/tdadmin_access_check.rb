user 'tdadmin' do
  comment 'tdadmin'
  password 'Passw0rd1!'
end

group "Administrators" do
  action :modify
  members "tdadmin"
  append true
end

group "Users" do
  action :modify
  excluded_members "tdadmin"
  append true
end
